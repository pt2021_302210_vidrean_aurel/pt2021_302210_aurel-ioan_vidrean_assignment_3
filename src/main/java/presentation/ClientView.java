package presentation;

import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ClientView extends JFrame {

    private JTextField nameTextField;
    private JTextField adresaTextField;
    private JTextField mailTextField;
    private JTextField idTextextField;
    private JLabel mailLabel;
    private JLabel adressLabel;
    private JLabel nameLabel;
    private JButton allClientsBtn;
    private JButton deleteBTN;
    private JButton editBtn;
    private JButton addBtn;


    public ClientView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 550, 300);
        setTitle("Client");
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        nameTextField = new JTextField();
        nameTextField.setBounds(30, 55, 86, 20);
        contentPane.add(nameTextField);
        nameTextField.setColumns(10);

        adresaTextField = new JTextField();
        adresaTextField.setBounds(185, 55, 86, 20);
        contentPane.add(adresaTextField);
        adresaTextField.setColumns(10);

        mailTextField = new JTextField();
        mailTextField.setBounds(325, 55, 86, 20);
        contentPane.add(mailTextField);
        mailTextField.setColumns(10);

        addBtn = new JButton("Add");
        addBtn.setBounds(30, 180, 89, 23);
        contentPane.add(addBtn);

        editBtn = new JButton("Edit");
        editBtn.setBounds(185, 180, 89, 23);
        contentPane.add(editBtn);

        deleteBTN = new JButton("Delete");
        deleteBTN.setBounds(325, 180, 89, 23);
        contentPane.add(deleteBTN);

        allClientsBtn = new JButton("View all");
        allClientsBtn.setBounds(435, 180, 89, 23);
        contentPane.add(allClientsBtn);

        nameLabel = new JLabel("Name:");
        nameLabel.setBounds(30, 30, 46, 14);
        contentPane.add(nameLabel);

        adressLabel = new JLabel("Adresa:");
        adressLabel.setBounds(185, 30, 46, 14);
        contentPane.add(adressLabel);

        mailLabel = new JLabel("Email:");
        mailLabel.setBounds(325, 30, 46, 14);
        contentPane.add(mailLabel);

        idTextextField = new JTextField();
        idTextextField.setBounds(30, 110, 86, 20);
        contentPane.add(idTextextField);
        idTextextField.setColumns(10);

        JLabel idLabel = new JLabel("ID:");
        idLabel.setBounds(30, 86, 46, 14);
        contentPane.add(idLabel);
    }

    public String getName(){
        return nameTextField.getText();
    }

    public String getAddress(){
        return adresaTextField.getText();
    }

    public String getEmail(){
        return mailTextField.getText();
    }

    public String getIdClient(){
        return idTextextField.getText();
    }

    public void setAddBtn(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }

    public void setEditBtn(ActionListener actionListener){
        this.editBtn.addActionListener(actionListener);
    }

    public void setDeleteBTN(ActionListener actionListener){
        this.deleteBTN.addActionListener(actionListener);
    }

    public void setAllClientsBtn(ActionListener actionListener){
        this.allClientsBtn.addActionListener(actionListener);
    }

}
