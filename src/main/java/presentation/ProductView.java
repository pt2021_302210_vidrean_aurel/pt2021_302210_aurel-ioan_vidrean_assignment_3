package presentation;

import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ProductView extends JFrame{

    private final JTextField nameTextField;
    private final JTextField pretTextField;
    private final JTextField stocTextField;
    private final JTextField idTextextField;
    private final JButton allProductsBtn;
    private final JButton deleteBTN;
    private final JButton editBtn;
    private final JButton addBtn;



    public ProductView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(750, 100, 550, 300);
        setTitle("Product");
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        nameTextField = new JTextField();
        nameTextField.setBounds(30, 55, 86, 20);
        contentPane.add(nameTextField);
        nameTextField.setColumns(10);

        pretTextField = new JTextField();
        pretTextField.setBounds(185, 55, 86, 20);
        contentPane.add(pretTextField);
        pretTextField.setColumns(10);

        stocTextField = new JTextField();
        stocTextField.setBounds(325, 55, 86, 20);
        contentPane.add(stocTextField);
        stocTextField.setColumns(10);

        addBtn = new JButton("Add");
        addBtn.setBounds(30, 180, 89, 23);
        contentPane.add(addBtn);

        editBtn = new JButton("Edit");
        editBtn.setBounds(185, 180, 89, 23);
        contentPane.add(editBtn);

        deleteBTN = new JButton("Delete");
        deleteBTN.setBounds(325, 180, 89, 23);
        contentPane.add(deleteBTN);

        allProductsBtn = new JButton("View all");
        allProductsBtn.setBounds(435, 180, 89, 23);
        contentPane.add(allProductsBtn);

        JLabel nameLabel = new JLabel("Name:");
        nameLabel.setBounds(30, 30, 46, 14);
        contentPane.add(nameLabel);

        JLabel pretLabel = new JLabel("Pret:");
        pretLabel.setBounds(185, 30, 46, 14);
        contentPane.add(pretLabel);

        JLabel stocLabel = new JLabel("Stoc:");
        stocLabel.setBounds(325, 30, 46, 14);
        contentPane.add(stocLabel);

        idTextextField = new JTextField();
        idTextextField.setBounds(30, 110, 86, 20);
        contentPane.add(idTextextField);
        idTextextField.setColumns(10);

        JLabel idLabel = new JLabel("ID:");
        idLabel.setBounds(30, 86, 46, 14);
        contentPane.add(idLabel);
    }

    public String getName(){
        return nameTextField.getText();
    }

    public String getPret(){
        return pretTextField.getText();
    }

    public String getStoc(){
        return stocTextField.getText();
    }

    public String getIdProdus(){
        return idTextextField.getText();
    }

    public void setAddBtn(ActionListener actionListener){
        this.addBtn.addActionListener(actionListener);
    }

    public void setEditBtn(ActionListener actionListener){
        this.editBtn.addActionListener(actionListener);
    }

    public void setDeleteBTN(ActionListener actionListener){
        this.deleteBTN.addActionListener(actionListener);
    }

    public void setAllClientsBtn(ActionListener actionListener){
        this.allProductsBtn.addActionListener(actionListener);
    }

}
