package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import java.util.List;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ClientController {

    private final ClientView view;

    public ClientController(ClientView view){
        this.view = view;
        ClientBLL clientBLL = new ClientBLL();

        this.view.setAddBtn(e -> {
            String nume = view.getName();
            String adresa = view.getAddress();
            String mail = view.getEmail();
            Client client = new Client(nume, adresa, mail);
            clientBLL.insertClient(client);
        });

        this.view.setEditBtn(e -> {
            String nume = view.getName();
            String adresa = view.getAddress();
            String mail = view.getEmail();
            try {
                int id = Integer.parseUnsignedInt(view.getIdClient());
                Client client = new Client(id, nume, adresa, mail);
                clientBLL.updateClient(client);
            }catch(NumberFormatException nfex){
                JOptionPane.showMessageDialog(view, "No valid ID");
            }
        });

        this.view.setDeleteBTN(e -> {
            try {
                int id = Integer.parseUnsignedInt(view.getIdClient());
                clientBLL.deleteClientById(id);
            } catch (NumberFormatException nfex) {
                JOptionPane.showMessageDialog(view, "No valid ID");
            }
        });

        this.view.setAllClientsBtn(e -> {
            List<Client> clientList = clientBLL.findAll();
            String[] columns;
            Object[][] data;
            columns = clientBLL.retrieveColumns();
            data = clientBLL.retrieveData();
            JFrame tableFrame=new JFrame("Client rows");
            JTable jT=new JTable(data, columns);
            JScrollPane scroll=new JScrollPane(jT);
            tableFrame.add(scroll);
            tableFrame.setBounds(500, 400, 300, 300);
            tableFrame.setVisible(true);
        });
    }

}
