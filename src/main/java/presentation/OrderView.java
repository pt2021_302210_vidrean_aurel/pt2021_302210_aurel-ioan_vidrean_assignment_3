package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class OrderView extends JFrame {

    private final JTextField clientIdTextField;
    private final JTextField productIdTextField;
    private final JTextField quantityTextField;
    private final JButton orderBtn;
    private final JButton viewAllBtn;

    public OrderView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 500,550, 300);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel idClientLabel = new JLabel("Client ID:");
        idClientLabel.setBounds(10, 11, 64, 19);
        contentPane.add(idClientLabel);

        clientIdTextField = new JTextField();
        clientIdTextField.setBounds(10, 40, 86, 20);
        contentPane.add(clientIdTextField);
        clientIdTextField.setColumns(10);

        JLabel icProdLabel = new JLabel("Product ID:");
        icProdLabel.setBounds(165, 13, 64, 17);
        contentPane.add(icProdLabel);

        productIdTextField = new JTextField();
        productIdTextField.setBounds(165, 40, 86, 20);
        contentPane.add(productIdTextField);
        productIdTextField.setColumns(10);

        JLabel quantityLabel = new JLabel("Quantity:");
        quantityLabel.setBounds(310, 13, 46, 14);
        contentPane.add(quantityLabel);

        quantityTextField = new JTextField();
        quantityTextField.setBounds(310, 40, 86, 20);
        contentPane.add(quantityTextField);
        quantityTextField.setColumns(10);

        orderBtn = new JButton("Place order");
        orderBtn.setBackground(Color.RED);
        orderBtn.setBounds(180, 150, 130, 50);
        contentPane.add(orderBtn);

        viewAllBtn = new JButton("View all");
        viewAllBtn.setBounds(350, 150, 100, 50);
        contentPane.add(viewAllBtn);
    }

    public String getClientId(){
        return clientIdTextField.getText();
    }

    public String getProductId(){
        return productIdTextField.getText();
    }

    public String getQuantity(){
        return quantityTextField.getText();
    }

    public void setOrderBtn(ActionListener actionListener){
        this.orderBtn.addActionListener(actionListener);
    }

    public void setViewAllBtn(ActionListener actionListener){
        this.viewAllBtn.addActionListener(actionListener);
    }

}
