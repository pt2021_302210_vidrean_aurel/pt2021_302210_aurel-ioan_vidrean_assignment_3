package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Ordershop;
import model.Product;

import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class OrderController {

    private OrderView view;

    public OrderController(OrderView view) {

        this.view = view;
        OrderBLL orderBLL = new OrderBLL();
        ProductBLL productBLL = new ProductBLL();
        ClientBLL clientBLL = new ClientBLL();

        this.view.setOrderBtn(e -> {
            try {
                int idClient = Integer.parseUnsignedInt(view.getClientId());
                int idProdus = Integer.parseUnsignedInt(view.getProductId());
                int quantity = Integer.parseUnsignedInt(view.getQuantity());
                Ordershop order = new Ordershop(idClient, idProdus, quantity);
                Product product = productBLL.findProductById(idProdus);
                if(product.getStoc() >= quantity) {
                    product.setStoc(product.getStoc()-quantity);
                    productBLL.updateProduct(product);
                    printBill(order, clientBLL, productBLL);
                    orderBLL.insertOrder(order);

                }
                else{
                    JOptionPane.showMessageDialog(view, "Not enough stock");
                }
            } catch (NumberFormatException nfex) {
                JOptionPane.showMessageDialog(view, "bad input");
            }
        });

        this.view.setViewAllBtn(e -> {
            List<Ordershop> orderList = orderBLL.findAll();
            String[] columns;
            Object[][] data;
            columns = orderBLL.retrieveColumns();
            data = orderBLL.retrieveData();
            JFrame tableFrame=new JFrame("Order rows");
            JTable jT=new JTable(data, columns);
            JScrollPane scroll=new JScrollPane(jT);
            tableFrame.add(scroll);
            tableFrame.setBounds(500, 400, 300, 300);
            tableFrame.setVisible(true);
        });

    }

    private void printBill(Ordershop order, ClientBLL clientBLL, ProductBLL productBLL)
    {
        PrintWriter writer;
        try {
            writer = new PrintWriter("Order.txt", StandardCharsets.UTF_8);
            Client client = clientBLL.findClientById(order.getIdClient());
            Product product = productBLL.findProductById(order.getIdProdus());
            writer.println("Order for: " + product.toString() + " requested by " + client.toString() + " is done.");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
