package presentation;

import bll.ProductBLL;
import model.Product;

import javax.swing.*;
import java.util.List;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ProductController {

    private final ProductView view;

    public ProductController(ProductView view){
        this.view = view;
        ProductBLL productBLL = new ProductBLL();

        this.view.setAddBtn(e -> {
            try {
                String nume = view.getName();
                int pret = Integer.parseUnsignedInt(view.getPret());
                int stoc = Integer.parseUnsignedInt(view.getStoc());
                Product product = new Product(nume, pret, stoc);
                productBLL.insertProduct(product);
            }catch(NumberFormatException nfex){
                JOptionPane.showMessageDialog(view,"bad input");
            }
        });

        this.view.setEditBtn(e -> {
            try {
                int id = Integer.parseUnsignedInt(view.getIdProdus());
                String nume = view.getName();
                int pret = Integer.parseUnsignedInt(view.getPret());
                int stoc = Integer.parseUnsignedInt(view.getStoc());
                Product product = new Product(id, nume, pret, stoc);
                productBLL.updateProduct(product);
            }catch(NumberFormatException nfex){
                JOptionPane.showMessageDialog(view, "No valid data");
            }
        });

        this.view.setDeleteBTN(e -> {
            try {
                int id = Integer.parseUnsignedInt(view.getIdProdus());
                productBLL.deleteProductById(id);
            } catch (NumberFormatException nfex) {
                JOptionPane.showMessageDialog(view, "No valid ID");
            }
        });

        this.view.setAllClientsBtn(e -> {
            List<Product> productList = productBLL.findAll();
            String[] columns;
            Object[][] data;
            columns = productBLL.retrieveColumns();
            data = productBLL.retrieveData();
            JFrame tableFrame=new JFrame("Product rows");
            JTable jT=new JTable(data, columns);
            JScrollPane scroll=new JScrollPane(jT);
            tableFrame.add(scroll);
            tableFrame.setBounds(500, 400, 300, 300);
            tableFrame.setVisible(true);
        });

    }

}
