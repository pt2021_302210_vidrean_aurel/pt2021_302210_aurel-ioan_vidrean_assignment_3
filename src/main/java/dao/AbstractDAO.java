package dao;

import connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public abstract class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Creating the select query by assembling the string " Select * from " table name " where " a specified field equals to a value
     * @param field field to choose
     * @return select after field string
     */

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE ").append(field).append("=?");
        return sb.toString();
    }

    /**
     * Creating the select query by assembling the string " Select * from " table name
     * @return select all stirng
     */

    private String createSelectAllQuery()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    /**
     * Creating the delete query by assembling the string " Delete from " table name " where id = " value
     * @return string to delete
     */

    private String createDeleteQuery(String field)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append("FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + "=?");
        return sb.toString();
    }

    /**
     * Creating the insert query by assembling the string "Insert into " table name + fields the values introduced by the user.
     * @return string to insert
     */

    private String createInsertQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ").append(type.getSimpleName()).append("(");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals("id")) {
                sb.append(field.getName()).append(",");
            }
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append(") VALUES(?,?,?)");
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Creating the update query by assembling the string " Update " table name " set " into the specified fields the values introduced by user.
     * @return string to update
     */

    private String createUpdateQuery() {
        StringBuilder sb = new StringBuilder();
        String id="";
        sb.append("UPDATE ");
        sb.append(type.getSimpleName()).append(" SET ");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals("id")) {
                sb.append(field.getName()).append("=?,");
            }
            else {
                id = field.getName();
            }
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append(" WHERE ").append(id).append("=?");
        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Creating the connection with the database and is established and then a query is executed
     * @return list of all rows
     */

    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Creating connection and executing the delete query specified by the user
     * @param id id ot be deleted
     * @return the deleted id
     */

    public int delete(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
            return 1;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:deleteById " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }

    /**
     * Finding the id specified. Method available for all three tables client, product, ordershop
     * @param id id to be searched for
     * @return the object
     */

    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            List<T> list = createObjects(resultSet);
            if (list.isEmpty()) {
                return null;
            } else{
                return list.get(0);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * creating the specified list of objects to be used further in the application
     * @param resultSet set of objects
     * @return obect to create
     */

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T) ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException | InvocationTargetException | SQLException | IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Inserting the t object intro to specified table from the data base
     * @param t object
     * @return string to insert
     */

    public T insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;

        String query = createInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            for (Field field : type.getDeclaredFields()) {
                if (!field.getName().equals("id")) {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    statement.setObject(i++,method.invoke(t));
                }
            }
            System.out.println(statement.toString());
            statement.executeUpdate();
        } catch (SQLException | IntrospectionException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert. " + e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * updating the table from database with specified data
     * @param t object
     * @return string to update
     */

    public T update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i=1;
            for (Field field : type.getDeclaredFields()) {
                if (!field.getName().equals("id")) {
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    statement.setObject(i++,method.invoke(t));
                }
            }
            for (Field field : type.getDeclaredFields()) {
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                Method method = propertyDescriptor.getReadMethod();
                statement.setObject(i++,method.invoke(t));
                break;
            }
            statement.executeUpdate();
        } catch (SQLException | IntrospectionException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert. " + e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return t;
    }

    /**
     * generete the column names to be written in JTable
     * @return fields
     */

    public String[] retrieveColumns(){
        String[] fields = new String[type.getDeclaredFields().length];
        int i=0;
        for(Field field:type.getDeclaredFields()){
            fields[i++]=field.getName();
        }
        return fields;
    }

    /**
     * retrieve the data from the table to be written in JTable
     * @return fields
     */

    public Object[][] retrieveData(){
        Object[][] fields = new Object[findAll().size()][];
        int i=0;
        for(T obj: findAll()){
            Object[] objects =new Object[type.getDeclaredFields().length];
            int j=0;
            for(Field field:type.getDeclaredFields()){
                try{
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getReadMethod();
                    objects[j++]=method.invoke(obj);
                } catch (IntrospectionException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
            fields[i++]=objects;
        }
        return fields;
    }

}
