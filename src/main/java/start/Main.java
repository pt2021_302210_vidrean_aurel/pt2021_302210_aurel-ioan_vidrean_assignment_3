package start;

import presentation.*;

import java.util.logging.Logger;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class Main {

    protected static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        ClientView clientView = new ClientView();
        clientView.setVisible(true);

        ProductView productView = new ProductView();
        productView.setVisible(true);

        OrderView orderView = new OrderView();
        orderView.setVisible(true);



        ClientController clientController = new ClientController(clientView);
        ProductController productController = new ProductController(productView);
        OrderController orderController = new OrderController(orderView);


    }
}
