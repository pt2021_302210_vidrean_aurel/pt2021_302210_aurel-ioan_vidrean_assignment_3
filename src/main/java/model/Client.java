package model;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class Client {

    private int id;
    private String nume;
    private String adresa;
    private String mail;

    public Client(){}

    public Client(int id, String nume, String adresa, String mail) {
        super();
        this.id = id;
        this.nume = nume;
        this.adresa = adresa;
        this.mail = mail;
    }

    public Client(String nume, String adresa, String mail) {
        super();
        this.nume = nume;
        this.adresa = adresa;
        this.mail = mail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public String toString() {
        return  nume + "";
    }
}
