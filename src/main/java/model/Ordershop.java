package model;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class Ordershop {

    private int id;
    private int idProdus;
    private int idClient;
    private int quantity;

    public Ordershop(){}

    public Ordershop(int id, int idProdus, int idClient, int quantity) {
        super();
        this.id = id;
        this.idProdus = idProdus;
        this.idClient = idClient;
        this.quantity = quantity;
    }

    public Ordershop(int idProdus, int idClient, int quantity) {
        super();
        this.idProdus = idProdus;
        this.idClient = idClient;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
