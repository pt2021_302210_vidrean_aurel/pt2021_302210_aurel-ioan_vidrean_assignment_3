package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.ClientDAO;

import model.Client;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ClientBLL {
    private ClientDAO clientDAO = new ClientDAO();
    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
    }

    /**
     * This method is serching the client using his unique id
     * @param id
     * @return
     */

    public Client findClientById(int id){
        Client st = clientDAO.findById(id);
        if(st == null){
            throw new NoSuchElementException("The client with id= " + id + " was not found!");
        }
        return st;
    }

    /**
     * Inserting a client by providing his atribuites
     * @param client
     * @return
     */
    public Client insertClient(Client client){
        for(Validator<Client> c : validators){
            c.validate(client);
        }
        return clientDAO.insert(client);
    }

    /**
     * Deleting the client with specified id
     * @param id
     * @return
     */

    public int deleteClientById(int id){
        Client st = clientDAO.findById(id);
        if(st == null){
            throw new NoSuchElementException("The client with id= " + id + " was not found!");
        }
        return clientDAO.delete(id);
    }

    /**
     * Finding client by id and updateing his atributes
     * @param client
     */

    public void updateClient(Client client){
        Client st = clientDAO.findById(client.getId());
        if(st == null){
            throw new NoSuchElementException("The client with id= " + client.getId() + " was not found!");
        }
        clientDAO.update(client);
    }

    /**
     * SELECT * FROM client
     * @return
     */
    public List<Client> findAll(){
        return clientDAO.findAll();
    }

    public String[] retrieveColumns(){
        return clientDAO.retrieveColumns();
    }

    public Object[][] retrieveData(){
        return clientDAO.retrieveData();
    }

}
