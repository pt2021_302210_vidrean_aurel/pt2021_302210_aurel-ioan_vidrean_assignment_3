package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class ProductBLL {
    private final ProductDAO productDAO = new ProductDAO();

    /**
     * This method is serching the product using his unique id
     * @param id
     * @return
     */

    public Product findProductById(int id){
        Product st = productDAO.findById(id);
        if(st == null){
            throw new NoSuchElementException("The product with id= " + id + " was not found!");
        }
        return st;
    }

    /**
     * Inserting a product by providing his atribuites
     * @param product
     */

    public void insertProduct(Product product){
        productDAO.insert(product);
    }

    /**
     * Deleting the product with specified id
     * @param id
     */

    public void deleteProductById(int id){
        Product st = productDAO.findById(id);
        if(st == null){
            throw new NoSuchElementException("The product with id= " + id + " was not found!");
        }
        productDAO.delete(id);
    }

    /**
     * Finding product by id and updateing his atributes
     * @param product
     */

    public void updateProduct(Product product){
        Product st = productDAO.findById(product.getId());
        if(st == null){
            throw new NoSuchElementException("The product with id= " + product.getId() + " was not found!");
        }
        productDAO.update(product);
    }

    /**
     * SELECT * FROM product
     * @return
     */

    public List<Product> findAll(){
        return productDAO.findAll();
    }

    public String[] retrieveColumns(){
        return productDAO.retrieveColumns();
    }

    public Object[][] retrieveData(){
        return productDAO.retrieveData();
    }

}