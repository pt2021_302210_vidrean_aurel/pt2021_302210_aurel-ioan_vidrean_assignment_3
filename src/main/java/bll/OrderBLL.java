package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.OrderDAO;
import model.Ordershop;

/**
 * @author: Aurel-Ioan Vidrean, student at Technical University of Cluj-Napoca, aurelvidrean16@gmail.com
 * @since 18/04/2021
 * @version 1.0.1
 * */

public class OrderBLL {
    private OrderDAO orderDAO = new OrderDAO();
    /**
     * Inserting an order by providing his atribuites
     * @param order
     */
    public void insertOrder(Ordershop order){
        orderDAO.insert(order);
    }

    /**
     * SELECT * FROM ordershop
     * @return
     */

    public List<Ordershop> findAll(){
        return orderDAO.findAll();
    }

    public String[] retrieveColumns(){
        return orderDAO.retrieveColumns();
    }

    public Object[][] retrieveData(){
        return orderDAO.retrieveData();
    }

}